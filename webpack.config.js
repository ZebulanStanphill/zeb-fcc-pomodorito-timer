/* global __dirname, module, require */
const path = require( 'path' );
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const HtmlWebpackTagsPlugin = require( 'html-webpack-tags-plugin' );

module.exports = {
	mode: 'production',
	entry: './src/main.tsx',
	output: {
		filename: 'bundle.js',
		path: path.resolve( __dirname, 'dist' )
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					'babel-loader'
				]
			},
			{
				test: /\.tsx?$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					'ts-loader'
				]
			},
			{
				test: /\.css$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					'style-loader',
					'css-loader'
				]
			},
			{
				test: /\.(oga|opus)$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					{
						loader: 'file-loader',
						options: {
							outputPath: 'assets'
						}
					}
				]
			},
			{
				test: /\.ttf$/,
				include: [
					path.resolve( __dirname, 'src' )
				],
				use: [
					{
						loader: 'file-loader',
						options: {
							outputPath: 'assets/fonts'
						}
					}
				]
			}
		]
	},
	resolve: {
		/* Without this, importing .tsx or .ts files without specifying the extension won't
		work, and TypeScript won't let you use explicit extensions in imports. */
		extensions: [ '.tsx', '.ts', '.jsx', '.js' ]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new CopyWebpackPlugin(
			[
				{ from: 'src/assets/style.css', to: 'style.css' }
			]
		),
		new HtmlWebpackPlugin( {
			template: './src/assets/main.html',
			xhtml: true
		} ),
		new HtmlWebpackTagsPlugin( {
			tags: [ 'style.css' ],
			append: true
		} )
	]
};
