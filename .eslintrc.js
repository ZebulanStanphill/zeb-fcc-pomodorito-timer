module.exports = {
	root: true,
	env: {
		browser: true,
		es6: true
	},
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:jsx-a11y/recommended',
		'plugin:react/recommended'
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaFeatures: {
			jsx: true
		},
		ecmaVersion: 2018,
		project: './tsconfig.json',
		sourceType: 'module'
	},
	plugins: [
		'@typescript-eslint',
		'jsx-a11y',
		'react',
		'react-hooks'
	],
	rules: {
		'@typescript-eslint/array-type': [
			'warn',
			'generic'
		],
		'@typescript-eslint/indent': [
			'warn',
			'tab'
		],
		'@typescript-eslint/no-var-requires': 'off',
		'@typescript-eslint/prefer-interface': 'off',
		'@typescript-eslint/member-delimiter-style': [
			'warn',
			{
				'multiline': {
					'delimiter': 'none',
					'requireLast': false
				},
				'singleline': {
					'delimiter': 'semi',
					'requireLast': false
				}
			}
		],
		'linebreak-style': [
			'error',
			'unix'
		],
		'quotes': [
			'warn',
			'single'
		],
		'semi': [
			'error',
			'always'
		],
		'jsx-a11y/media-has-caption': 'off',
		'react/destructuring-assignment': 'warn',
		'react/display-name': 'off',
		'react/jsx-uses-react': 'error',
		'react/no-access-state-in-setstate': 'error',
		'react/no-danger-with-children': 'error',
		'react/no-did-mount-set-state': 'error',
		'react/no-did-update-set-state': 'error',
		'react/no-redundant-should-component-update': 'error',
		'react/no-this-in-sfc': 'error',
		'react/no-unused-state': 'error',
		'react/no-will-update-set-state': 'error',
		'react/prefer-es6-class': 'error',
		'react/prefer-stateless-function': 'error',
		'react/prop-types': 'off',
		'react/jsx-filename-extension': [
			'warn',
			{ 'extensions': [ '.jsx', '.tsx' ] }
		],
		'react-hooks/rules-of-hooks': 'error'
	},
	settings: {
		'react': {
			'pragma': 'h',
			'version': '16.8'
		}
	}
};