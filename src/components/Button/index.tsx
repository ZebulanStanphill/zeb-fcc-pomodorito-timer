// External dependencies
import classNames from 'classnames';
import { createElement as h, memo, ReactNode } from 'react';

// Internal dependencies
import './style.css';

type Props = {
	children?: ReactNode
	class?: string
	id?: string
	onClick: () => void
	primary?: boolean
	small?: boolean
	type?: 'button' | 'reset' | 'submit'
}

export default memo( function Button( {
	children,
	class: className,
	id,
	onClick,
	primary = false,
	small = false,
	type = 'button'
}: Props ): JSX.Element {
	return (
		<button
			className={ classNames(
				'Pomodorito-Button',
				primary ? 'Pomodorito-Button--primary' : 'Pomodorito-Button--regular',
				small ? 'Pomodorito-Button--small' : 'Pomodorito-Button--medium',
				className
			) }
			id={ id }
			onClick={ onClick }
			type={ type }
		>
			{ children }
		</button>
	);
} );