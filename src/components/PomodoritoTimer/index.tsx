// External dependencies
import { createElement as h, useReducer, useRef } from 'react';
//import useInterval from 'use-interval';

// Project component dependencies
import Button from '../Button';
import PeriodLengthEditor from '../PeriodLengthEditor';

// Project hook dependencies
import useLayoutInterval from '../../hooks/useLayoutInterval';

// Project asset dependencies
// Sound from https://freesound.org/people/keweldog/sounds/181148/
import beepSound from './assets/beep.oga';

// Internal dependencies
import './style.css';

// Reducer action constants (to prevent uncaught string typos).
const PAUSE = 'PAUSE';
const SET_PERIOD_LENGTH = 'SET_PERIOD_LENGTH';
const SET_TIME_REMAINING = 'SET_TIME_REMAINING';
const START = 'START';
const TOGGLE_PERIOD ='TOGGLE_PERIOD';
const RESET = 'RESET';

function millisecondsToMMSS( ms: number ): string {
	const totalSeconds = Math.round( ms / 1000 );
	const minutes = Math.floor( totalSeconds / 60 );
	const seconds = totalSeconds - minutes * 60;

	return minutes.toString().padStart( 2, '0' ) + ':' + seconds.toString().padStart( 2, '0' );
}

export default function PomodoritoTimer( {
	defaultBreakLength = 5,
	breakLengthMin = 1,
	breakLengthMax = 60,
	defaultSessionLength = 25,
	sessionLengthMin = 1,
	sessionLengthMax = 60
} ): JSX.Element {
	type ReducerState = {
		activityStatus: 'paused' | 'stopped' | 'running'
		breakLength: number
		breakLengthMax: number
		breakLengthMin: number
		isBreak: boolean
		msRemaining: number
		sessionLength: number
		sessionLengthMax: number
		sessionLengthMin: number
	}

	const initialState: ReducerState = {
		activityStatus: 'stopped', // Whether timer is running, paused, or not started yet.
		isBreak: false, // Whether the current period is a break or a session.
		msRemaining: defaultSessionLength * 60000, // Timer value in milliseconds.
		breakLength: defaultBreakLength, // Break length in minutes.
		breakLengthMin,
		breakLengthMax,
		sessionLength: defaultSessionLength, // Session length in minutes.
		sessionLengthMin,
		sessionLengthMax
	};

	type PauseAction = {
		type: 'PAUSE'
	}

	type ResetAction = {
		type: 'RESET'
	}

	type SetPeriodLengthAction = {
		type: 'SET_PERIOD_LENGTH'
		period: 'break' | 'session'
		newValue: number
	}

	type SetTimeRemainingAction = {
		type: 'SET_TIME_REMAINING'
		newValue: number
	}

	type StartAction = {
		type: 'START'
	}

	type TogglePeriodAction = {
		type: 'TOGGLE_PERIOD'
	}

	type ReducerAction = PauseAction | ResetAction | SetPeriodLengthAction | SetTimeRemainingAction | StartAction | TogglePeriodAction;

	function reducer( state: ReducerState, action: ReducerAction ): ReducerState {
		switch ( action.type ) {
			case TOGGLE_PERIOD:
				return {
					...state,
					isBreak: ! state.isBreak
				};
			case SET_PERIOD_LENGTH:
				if (
					action.period === 'break' &&
					action.newValue >= state.breakLengthMin &&
					action.newValue <= state.breakLengthMax
				) {
					return {
						...state,
						breakLength: action.newValue
					};
				} else if (
					action.period === 'session' &&
					action.newValue >= state.sessionLengthMin &&
					action.newValue <= state.sessionLengthMax
				) {
					if ( state.activityStatus === 'stopped' ) // If timer has not yet started.
						return {
							...state,
							sessionLength: action.newValue,
							msRemaining: action.newValue * 60000
						};
					else // If timer is running or paused.
						return {
							...state,
							sessionLength: action.newValue
						};
				}
				return state;
			case START:
				return {
					...state,
					activityStatus: 'running'
				};
			case PAUSE:
				return {
					...state,
					activityStatus: 'paused'
				};
			case SET_TIME_REMAINING:
				return {
					...state,
					msRemaining: action.newValue
				};
			case RESET:
				return initialState;
			default: // If the disatched action was unknown/invalid.
				return state;
		}
	}

	const [
		{
			activityStatus, // Whether timer is running, paused, or not started yet.
			breakLength, // Break length in minutes.
			isBreak, // Whether the current period is a break or a session.
			msRemaining, // Timer value in milliseconds.
			sessionLength // Session length in minutes.
		},
		dispatch
	] = useReducer( reducer, initialState );

	const audioRef = useRef<HTMLAudioElement>( null );

	useLayoutInterval(
		(): void => {
			if ( activityStatus === 'running' ) {
				// If the timer has not yet hit 0 during its current session or break...
				if ( msRemaining > 0 )
					dispatch( {
						type: SET_TIME_REMAINING,
						newValue: msRemaining - 1000
					} );
				// If the timer has hit 0...
				else {
					if ( audioRef.current !== null )
						audioRef.current.play();
					// If it is currently a break, reset the timer for a session.
					if ( isBreak )
						dispatch( {
							type: SET_TIME_REMAINING,
							newValue:
								sessionLength * 60000 // Convert minutes to milliseconds.
						} );
					// If it is currently a session, reset the timer for a break.
					else {
						dispatch( {
							type: SET_TIME_REMAINING,
							newValue:
								breakLength * 60000 // Convert minutes to milliseconds.
						} );
					}
					// Calling this before or after the earlier dispatch calls does not change the
					// result, as the value of isBreak used by dispatch has not yet actually changed.
					// (I think.)
					dispatch( { type: TOGGLE_PERIOD } );
				}
			}
		},
		activityStatus === 'running' ? 1000 : null
	);

	return (
		<div className="Pomodorito-PomodoritoTimer">
			<h1>Zeb’s Pomodorito Timer</h1>
			<PeriodLengthEditor
				period="session"
				label="Session Length"
				length={ sessionLength }
				dispatch={ dispatch }
			/>
			<PeriodLengthEditor
				period="break"
				label="Break Length"
				length={ breakLength }
				dispatch={ dispatch }
			/>
			<span className="Pomodorito-PomodoritoTimer__time-label" id="timer-label">
				{ isBreak ? 'Break Time' : 'Session Time' }
			</span>
			<span className="Pomodorito-PomodoritoTimer__time-left" id="time-left">
				{ millisecondsToMMSS( msRemaining ) }
			</span>
			<div className="Pomodorito-PomodoritoTimer__buttons">
				<Button
					id="start_stop"
					onClick={ (): void => {
						// If timer is stopped or paused, start/resume it.
						if (
							activityStatus === 'paused' ||
							activityStatus === 'stopped'
						) {
							dispatch( { type: START } );
						}
						// If timer is running, pause it.
						else {
							dispatch( { type: PAUSE } );
						}
					} }
				>
					Start/Stop
				</Button>
				<Button
					id="reset"
					onClick={ (): void => {
						if ( audioRef.current !== null ) {
							audioRef.current.pause();
							audioRef.current.currentTime = 0;
						}
						dispatch( { type: RESET } );
					} }
				>
					Reset
				</Button>
			</div>
			<audio id="beep" ref={ audioRef }>
				<source src={ beepSound } type="audio/ogg" />
			</audio>
		</div>
	);
}