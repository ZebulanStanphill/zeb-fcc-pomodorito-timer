// External dependencies
import { createElement as h, memo } from 'react';

// Project component dependencies
import Button from '../Button';

// Internal dependencies
import './style.css';

type Props = {
	dispatch: ( action: object ) => void
	label: string
	length: number
	period: string
}

// https://reactjs.org/docs/forwarding-refs.html#forwarding-refs-to-dom-components
export default memo( function PeriodLengthEditor( {
	dispatch,
	label,
	length,
	period
}: Props ): JSX.Element {
	return (
		<div className="Pomodorito-PeriodLengthEditor">
			<span
				className="Pomodorito-PeriodLengthEditor__label"
				id={ period + '-label' }
			>
				{ label + ':' }
			</span>
			<span className="Pomodorito-PeriodLengthEditor__length">
				<span id={ period + '-length' }>{ length }</span>
				{ length > 1 ? ' minutes' : ' minute' }
			</span>
			<div className="Pomodorito-PeriodLengthEditor__buttons">
				<Button
					id={ period + '-decrement' }
					onClick={ (): void => dispatch( {
						type: 'SET_PERIOD_LENGTH',
						period: period,
						newValue: length - 1
					} ) }
				>
					Decrease
				</Button>
				<Button
					id={ period + '-increment' }
					onClick={ (): void => dispatch( {
						type: 'SET_PERIOD_LENGTH',
						period: period,
						newValue: length + 1
					} ) }
				>
					Increase
				</Button>
			</div>
		</div>
	);
} );