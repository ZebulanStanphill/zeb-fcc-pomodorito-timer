// External dependencies
import { createElement as h } from 'react';

// Project component dependencies
import Footer from '../Footer';
import PomodoritoTimer from '../PomodoritoTimer';

// Internal dependencies
import './style.css';

export default function App(): JSX.Element {
	return (
		<div className="ZebPomodoritoApp">
			<PomodoritoTimer />
			<Footer />
		</div>
	);
}