// External dependencies
import { useLayoutEffect, useRef } from 'react';

// Based on https://github.com/Hermanya/use-interval which is released under the MIT License.
export default function useLayoutInterval( callback: () => void, delay: number | null ): void {
	const savedCallback = useRef( (): void => {} );

	// Remember the latest callback.
	useLayoutEffect( (): void => {
		savedCallback.current = callback;
	}, [ callback ] );

	// Set up the interval.
	useLayoutEffect( (): VoidFunction => {
		function tick(): void {
			savedCallback.current();
		}
		if ( delay !== null ) {
			const id = setInterval( tick, delay );

			return (): void => {
				clearInterval( id );
			};
		} else {
			return (): void => {};
		}
	}, [ delay ] );
}